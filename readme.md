# astrocling's Dotfiles

A collection of dotfiles and scripts for use in Linux/Bash to make life a little
easier.

## Getting Started

To get this running you just need to clone the repository into your home 
directory.  This will create the dotfiles directory.  You can run dotfiles.sh 
to install the dotfiles.  Keys.sh can be used to install the authorized keys 
file, but I'd recommend using your own authorized keys file as I probably don't 
need access to your server.

Be aware that the .bashrc file will automatically do a git pull on this
repository everytime that you login to shell.  You may want to comment that out.


### Prerequisites

This should work on most Linux instances with Vim or even MacOS if you're
regularly using the command line there.


### Installing

Clone the repository into your home directory.

```
Run ~/dotfiles.sh
```


## Authors

* **Bruce Clingan** - *Initial work* - 
[astrocling](https:/bitbucket.org/astrocling)

## License

This project is licensed under the MIT License - see the 
[license.md](LICENSE.md) file for details

## Acknowledgments


