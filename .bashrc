# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias keys='~/dotfiles/keys.sh'             # Apply authorized keys from repo
alias dots='cd ~/dotfiles && git pull'      # Pull dotfiles from repo

alias vi=vim                                # Use Vim instead of vi
alias svi='sudo vi'                         # Sudo Vim the file
alias vis='vim "+set si"'                   # Set Vim
alias edit='vim'                            # Edit Opens Vim on a file

alias ip='hostname --ip-address'            # Find External Ip Address

alias gs='git status'                       # Git Status
alias ga='git add .'                        # Git Add all
alias gl='git log'                          # Git Log
alias gc='git commit'                       # Git Commit

alias bpr='source ~/.bash_profile'          # Reload Bash Profile

alias ll='ls -la'                           # List Long All
alias la='ls -a'                            # List All

alias cd..='cd ../'                         # Go back 1 directory level (for fast typers)
alias ..='cd ../'                           # Go back 1 directory level
alias ...='cd ../../'                       # Go back 2 directory levels
alias .3='cd ../../../'                     # Go back 3 directory levels
alias .4='cd ../../../../'                  # Go back 4 directory levels
alias .5='cd ../../../../../'               # Go back 5 directory levels
alias .6='cd ../../../../../../'            # Go back 6 directory levels

export PATH="$HOME//vendor/bin:$PATH"
export PATH="usr/local/bin/composer:$PATH" 


#Pull Latest Dotfiles
echo "Pulling latest Dotfiles"
cd ~/dotfiles && git pull && cd ~/
echo "Dotfiles up to date"
