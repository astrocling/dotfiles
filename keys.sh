#!/bin/bash
############################
# .make.sh
# This script creates symlink and backs up old authorizedkeys file 
############################

########## Variables

dir=~/dotfiles                    # dotfiles directory
olddir=~/dotfiles_old             # old dotfiles backup directory

##########

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks 
    echo "Moving any existing authorized keys from ~ to $olddir"
    mv ~/.ssh/authorizedkeys ~/dotfiles_old/authorizedkeys.old
    echo "Creating symlink to authorized keys in .ssh directory."
    ln -s "$dir"/authorizedkeys.server ~/.ssh/authorizedkeys

