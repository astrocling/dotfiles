# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

alias vi=vim
alias svi='sudo vi'
alias vis='vim "+set si"'
alias edit='vim'

export PATH
